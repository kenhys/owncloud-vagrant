# ownCloud development environment

## Prerequisite

* Vagrant
* Virtualbox
* Ansible

## Setup

1. Get ownCloud core source code
    ```
    $ git clone https://github.com/owncloud/core
    $ cd core
    $ git checkout stable10
    ```
    We can use stable10 branch because Debian stretch uses PHP7.0.
    The master branch requires PHP7.1.
1. Start the virtual machine
    ```
    $ vagrant up
    ```
1. Setup ownCloud core
    ```
    $ vagrant ssh
    vagrant $ sudo -u www-data -H bash -l
    www-data $ cd /var/www/owncloud
    www-data $ nvm install 10
    www-data $ make clean && make
    ```
1. Run installer
   * Access http://localhost:8080/owncloud via web browser
     * DB name: `arbitrary`
     * DB user: `owncloud`
     * DB password: `owncloud`
   * Or run installer via CLI
       ```
       www-data $ php ./occ maintenance:install \
          --data-dir /var/lib/owncloud/data \
          --database "mysql" \
          --database-name "owncloud" \
          --database-user "owncloud" \
          --database-pass "owncloud" \
          --admin-user "admin" \
          --admin-pass "password"
       ```

## Add applications

Mount `./apps` to `/var/www/owncloud-apps` via Vagrantfile.

For example, add calendar:

```
$ cd ./apps
$ git clone https://github.com/owncloud/calendar
```

Build the application:

```
vagrant $ sudo -u www-data -H bash -l
www-data $ cd /var/www/owncloud/apps
www-data $ ln -s /var/www/owncloud-apps/calendar calendar
www-data $ cd calendar
www-data $ npm install -g yarn
www-data $ make
```

Enable the application:

```
www-data $ cd /var/www/owncloud
www-data $ php ./occ app:enable calendar
```

We can use calendar application.

We must run `make` to apply changes if we change `apps/calendar/*.js`.

```
www-data $ cd /var/www/owncloud/apps/calendar
www-data $ make
```

We must reload the page to apply changes if we change `*.php`.


