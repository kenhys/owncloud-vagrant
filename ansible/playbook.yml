- hosts: servers
  become: yes
  vars:
    nvm_version: v0.33.11
    node_version: "10"
  tasks:
    - name: Upgrade packages
      apt:
        update_cache: yes
        cache_valid_time: 3600
        upgrade: safe
    - name: Install base packages
      apt:
        name: "{{ item }}"
      with_items:
        - apt-transport-https
        - apt-listchanges
        - apticron
        - bridge-utils
        - curl
        - vim
        - zsh
        - git
        - lv
        - python-mysqldb
        - python-psycopg2
        - ruby
        - rsync

    - name: Install ownCloud dependencies
      apt:
        name: "{{ item }}"
      with_items:
        - mariadb-server
        - postgresql-all
        - sqlite3
        - php-apcu
        - php-apcu-bc
        - php-curl
        - php-gd
        - php-imap
        - php-intl
        - php-json
        - php-ldap
        - php-mbstring
        - php-mcrypt
        - php-mysql
        - php-smbclient
        - php-ssh2
        - php-sqlite3
        - php-xml
        - php-zip

    - name: Setup MariaDB user for ownCloud
      mysql_user:
        name: owncloud
        password: owncloud
        priv: "*.*:ALL,GRANT"
        state: present

    - name: Setup PostgreSQL user for ownCloud
      become: yes
      become_user: postgres
      postgresql_user:
        name: owncloud
        password: owncloud
        role_attr_flags: SUPERUSER
      vars:
        ansible_ssh_pipelining: true

    - name: Change owner/group /var/www
      file:
        path: /var/www
        owner: www-data
        group: www-data
        state: directory

    - name: Setup NVM for www-data
      git:
        repo: https://github.com/creationix/nvm.git
        dest: /var/www/.nvm
        version: "{{ nvm_version }}"
    - name: Check /var/www/.nvm/versions
      stat:
        path: /var/www/.nvm/versions
      register: nvm_versions
    - name: Change owner/group /var/www/.nvm
      file:
        path: /var/www/.nvm
        owner: www-data
        group: www-data
        state: directory
        recurse: true
      when: not nvm_versions.stat.exists
    - name: Setup NVM for vagrant
      become: vagrant
      git:
        repo: https://github.com/creationix/nvm.git
        dest: /home/vagrant/.nvm
        version: "{{ nvm_version }}"
    - name: Copy nvm.sh
      copy:
        src: etc/profile.d/nvm.sh
        dest: /etc/profile.d/nvm.sh
    - name: Install Node.js
      become: www-data
      shell: |
        bash -l -c 'nvm install {{ node_version }}'
    - name: Prepare /var/lib/owncloud/data
      file:
        path: /var/lib/owncloud/data
        owner: www-data
        group: www-data
        state: directory
        recurse: yes

    - name: Install Apache and related packages
      apt:
        name: "{{ item }}"
      with_items:
        - apache2
        - libapache2-mod-php

    - name: Put Apache configuration file
      copy:
        src: etc/apache2/sites-available/{{ item }}
        dest: /etc/apache2/sites-available/{{ item }}
      with_items:
        - 000-default.conf
      register: apache_config_files

    - name: Enable our Apache configurations
      command: a2ensite {{ item }}
      with_items:
        - 000-default
      notify:
        - Restart Apache
      when: apache_config_files.changed

    - name: Put Apache modules configuration file
      copy:
        src: etc/apache2/mods-available/{{ item }}
        dest: /etc/apache2/mods-available/{{ item }}
      with_items:
        - mpm_prefork.conf
    - name: Disable Apache modules
      command: a2dismod {{ item }}
      with_items:
        - mpm_event
        - mpm_worker
    - name: Enable Apache modules
      command: a2enmod {{ item }}
      with_items:
        - dir
        - env
        - headers
        - mime
        - mpm_prefork
        - proxy
        - proxy_http
        - rewrite
        - ssl
      notify:
        - Restart Apache

  handlers:
    - name: Restart Apache
      service:
        name: apache2
        state: restarted
